var drawModule = ( function() {

  var snakeBody = function(x,y) {
    // A single segment of the snake's body
    ctx.fillStyle = 'green';
    ctx.fillRect( x * snakeSize, y * snakeSize, snakeSize, snakeSize);
    // Border of a snake segment
    ctx.strokeStyle = 'darkgreen';
    ctx.strokeRect(x * snakeSize, y * snakeSize, snakeSize, snakeSize);
  }

  var pizza = function(x,y) {
    ctx.fillStyle = 'yellow';
    ctx.fillRect( x * snakeSize, y * snakeSize, snakeSize, snakeSize);
    ctx.strokeStyle = 'red';
    ctx.strokeRect( x * snakeSize+1, y * snakeSize+1, snakeSize-2, snakeSize-2);
  }

  var scoreBoard = function() {
    var score_board = "Score: " + score;
    ctx.fillStyle = 'blue';
    ctx.fillText(score_board, 145, height - 5);
  }

  var drawSnake = function() {
    // Baby snake is 4 tiles
    var snakeLength = 4;
    snake = [];

    for ( var i = snakeLength; i>0; i--) {
      snake.push({x:i, y:0});
    }
  }

  var spawnFood = function() {
    food = {
      x: Math.floor((Math.random() * 30) +1),
      y: Math.floor((Math.random() * 30) +1)
    };

    // Check we're not spawning food in the snakeSize
    for ( var i = 0; i > snake.length; i++) {
      var segX = snake[i].x;
      var segY = snake[i].y;

      if( (food.x === segX && food.y === segY) || food.x === segX || food.y === segY) {
        // re-roll the food spawn
        food.x = Math.floor((Math.random() * 30) +1);
        food.y = Math.floor((Math.random() * 30) +1);
      }
    }
  }

  var checkCollision = function(x,y,snake) {
    for ( var i = 0; i < snake.length; i++) {
      if ( snake[i].x === x && snake[i].y === y) {
        return true;
      }
    }
    return false;
  }

  var paint = function () {
    // generic tile
    ctx.fillStyle = 'lightgrey';
    ctx.fillRect(0,0,width,height);
    ctx.strokeStyle = 'black';
    ctx.strokeRect(0,0,width,height);

    startButton.setAttribute('disabled', true);

    var snakeX = snake[0].x;
    var snakeY = snake[0].y;

    // Find out which way they're moving...
    switch( direction ) {
      case 'right' : snakeX++; break;
      case 'left' : snakeX--; break;
      case 'up' : snakeY--; break;
      case 'down' : snakeY++; break;
    }

    // they hit the wall, or themselves... They lost!
    if ( snakeX == -1 || snakeX == (width/snakeSize) || snakeY == -1 || snakeY == (height/snakeSize) || checkCollision(snakeX, snakeY, snake)) {
      // Restart game
      if (highScore < score ) {
        highScore = score;
        document.getElementById('highScore').innerHTML = highScore;
      }
      score = 0;
      startButton.removeAttribute('disabled', true);
      ctx.clearRect(0,0,width,height);
      gameloop = clearInterval(gameloop);
      return;
    }

    // if they got food we need to make them bigger and spawn more food
    if (snakeX == food.x && snakeY == food.y) {
      var tail = {
        x: snakeX,
        y: snakeY
      };

      score++;

      spawnFood();
    } else {
      // we just move the snake along
      var tail = snake.pop();
      tail.x = snakeX;
      tail.y = snakeY;
    }

    snake.unshift(tail);

    for (var i = 0; i < snake.length; i++) {
      snakeBody(snake[i].x, snake[i].y);
    }

    pizza(food.x, food.y);

    scoreBoard();
  }

  var init = function () {
    direction = 'down';
    drawSnake();
    spawnFood();
    gameloop = setInterval(paint, gameSpeed);
  }

  return {
    init: init
  };

// Implicit init for the module
}());
